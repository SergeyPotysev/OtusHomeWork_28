﻿using Microsoft.SmallBasic.Library;
using System;

namespace Prototype
{
    [Serializable]
    class Rhombus : Figure
    {
        public Rhombus() : base()
        {
            
        }


        public override void Plot()
        {
            int half = Width / 2;
            GraphicsWindow.DrawLine(X + half, Y, X + Width, Y + half);
            GraphicsWindow.DrawLine(X + Width, Y + half, X + half, Y + Width);
            GraphicsWindow.DrawLine(X + half, Y + Width, X, Y + half);
            GraphicsWindow.DrawLine(X, Y + half, X + half, Y);
        }


        public override void Export(IExport export)
            => export.Export(this);
    }
}

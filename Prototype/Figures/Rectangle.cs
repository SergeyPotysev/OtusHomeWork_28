﻿using Microsoft.SmallBasic.Library;
using System;

namespace Prototype
{
    [Serializable]
    class Rectangle : Figure
    {
        public Rectangle() : base()
        { 
        
        }


        public override void Plot()
            => GraphicsWindow.DrawRectangle(X, Y, Width, Height);


        public override void Export(IExport export)
            => export.Export(this);
    }
}

﻿using Microsoft.SmallBasic.Library;
using System;

namespace Prototype
{
    [Serializable]
    class Triangle : Figure
    {
        public Triangle() : base()
        {    
            
        }


        public override void Plot()
            => GraphicsWindow.DrawTriangle(X, Y, X + Width, Y, X + Width / 2, Y + Height);


        public override void Export(IExport export)
            => export.Export(this);
    }
}

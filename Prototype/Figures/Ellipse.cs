﻿using Microsoft.SmallBasic.Library;
using System;

namespace Prototype
{
    [Serializable]
    class Ellipse : Figure
    {
        public Ellipse() : base()
        {
        
        }


        public override void Plot()
            => GraphicsWindow.DrawEllipse(X, Y, Width, Height);


        public override void Export(IExport export)
            => export.Export(this);
    }
}
﻿using Microsoft.SmallBasic.Library;
using System;

namespace Prototype
{
    [Serializable]
    public class Point : IMyCloneable<Point>
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Point()
        {
            X = 0;
            Y = 0;
        }

        /// <summary>
        /// Присвоить свойству X объекта Point значение.
        /// </summary>
        public Point SetX(int x)
        {
            X = x;
            return this;
        }


        /// <summary>
        /// Присвоить свойству Y объекта Point значение.
        /// </summary>
        public Point SetY(int y)
        {
            Y = y;
            return this;
        }

        /// <summary>
        /// Нарисовать точку.
        /// </summary>
        public void Draw()
            => GraphicsWindow.SetPixel(X, Y, "White");

        /// <summary>
        /// Скрыть точку.
        /// </summary>
        public void Hide()
            => GraphicsWindow.SetPixel(X, Y, SetDrawer.GetInstance().BackgroundColor);


        /// <summary>
        /// Неполное копирование (Shallow copy).
        /// </summary>
        /// <returns>Клонированный объект.</returns>
        public Point MyClone()
            => MemberwiseClone() as Point;
    }
}
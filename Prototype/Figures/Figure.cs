﻿using Microsoft.SmallBasic.Library;
using System;

namespace Prototype
{
    [Serializable]
    abstract class Figure
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public Figure()  
        {
            Width = SetDrawer.GetInstance().FieldSize;
            Height = 7 * Width / 10;
            GraphicsWindow.PenWidth = 2;
            GraphicsWindow.BrushColor = SetDrawer.GetInstance().BackgroundColor;
        }


        /// <summary>
        /// Задать фигуре начальную координату по оси X.
        /// </summary>
        /// <param name="x">Новое значение координаты по оси X.</param>
        public Figure SetX(int x)
        {
            X = x;
            return this;
        }


        /// <summary>
        /// Задать фигуре начальную координату по оси Y.
        /// </summary>
        /// <param name="y">Новое значение координаты по оси Y.</param>
        public Figure SetY(int y)
        {
            Y = y;
            return this;
        }


        public abstract void Plot();

        public virtual void Export(IExport export)
            => export.Export(this);
    }
}
﻿using System;
using System.Collections.Generic;

namespace Prototype
{
    [Serializable]
    class ColourType : Parameter
    {
        public List<string> items; 

        public ColourType()
        {
            items = new List<string>() { "Red", "Blue", "Green", "Yellow" };
        }

 
        public override void Change()
        {
            var rndColor = new Random();
            int colorIndex;
            // Prototype pattern
            var colourNow = new ColourType().Clone() as ColourType;
            foreach (Letter letter in Program.letters)
            {
                colorIndex = rndColor.Next(colourNow.items.Count);
                // Builder pattern. Задать цвет букве
                letter.SetColor(colourNow.items[colorIndex]);
                colourNow.items.RemoveAt(colorIndex);
            }
        }
    }

}

﻿using System;
using System.Collections.Generic;

namespace Prototype
{
    [Serializable]
    class FigureType : Parameter
    {
        public List<Figure> items;

        public FigureType()
        {
            items = new List<Figure>() { new Rectangle(), new Ellipse(), new Rhombus(), new Triangle() };
        }


        public override void Change()
        {
            var rndFigure = new Random();
            int figureIndex;
            // Prototype pattern
            var figureNow = new FigureType().Clone() as FigureType;
            foreach (Letter letter in Program.letters)
            {
                figureIndex = rndFigure.Next(figureNow.items.Count);
                // Builder pattern. Задать тип фигуры
                letter.SetFigure(figureNow.items[figureIndex]);
                figureNow.items.RemoveAt(figureIndex);
            }
        }
    }

}

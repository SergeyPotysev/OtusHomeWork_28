﻿using Microsoft.Extensions.Configuration;
using Microsoft.SmallBasic.Library;
using System;
using System.Collections.Generic;
using System.IO;

namespace Prototype
{
    class SetDrawer : IMyCloneable<SetDrawer>
    {
        /// <summary>
        /// Объект, содержащий начальные установки.
        /// </summary>
        private static SetDrawer _settings = null;

        /// <summary>
        /// Заголовок окна.
        /// </summary>
        public static string Title { get; private set; }

        /// <summary>
        ///  Рисунок шрифта объектов - букв.
        /// </summary>
        public static string MapFile { get; private set; }

        /// <summary>
        /// Координата расположения окна относительно экрана по оси X.
        /// </summary>
        public static int WindowTop { get; private set; }

        /// <summary>
        /// Координата расположения окна относительно экрана по оси Y.
        /// </summary>
        public static int WindowLeft { get; private set; }

        /// <summary>
        /// Ширина окна.
        /// </summary>
        public static int WindowWidth { get; private set; }

        /// <summary>
        /// Высота окна.
        /// </summary>
        public static int WindowHeight { get; private set; }

        /// <summary>
        /// Цвет фона окна.
        /// </summary>
        public string BackgroundColor { get; private set; }

        /// <summary>
        /// Размер поля.
        /// </summary>
        public int FieldSize { get; private set; }

        /// <summary>
        /// Координата X верхнего левого угла первой буквы относительно вернего левого угла окна. 
        /// </summary>
        public int StartX { get; private set; }

        /// <summary>
        /// Координата Y верхнего левого угла первой буквы относительно вернего левого угла окна.
        /// </summary>
        public int StartY { get; private set; }

        /// <summary>
        /// Величина смещения клона по оси X.
        /// </summary>
        public static int CloneOffsetX { get; private set; }

        /// <summary>
        /// Задержка в миллисекундах между отрисовками.
        /// </summary>
        public int Delay { get; private set; }

        /// <summary>
        /// Текущий способ сериализации клонированных объектов.
        /// </summary>
        public static string SerializationType { get; private set; }

        /// <summary>
        /// Высота надписи с элементами управления.
        /// </summary>
        public static int GroundFloor { get; private set; }

        /// <summary>
        /// Текущий тип фигуры для сериализации объекта с таким типом.
        /// </summary>
        public Figure CurrentFigure { get; private set; }

        /// <summary>
        /// Индекс перебора значений в наборе объектов для выбора текущего типа фигуры.
        /// </summary>
        private static int _figureIndex;

        /// <summary>
        /// Набор типов сериализации.
        /// </summary>
        private static readonly string[] _serializationType = { "JSON", "XML" };

        /// <summary>
        /// Титры.
        /// </summary>
        public static string Credits { get; private set; }


        private SetDrawer()
        {
            IConfiguration AppConfiguration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();
            Title = Convert.ToString(AppConfiguration.GetSection("Title").Value);
            MapFile = Convert.ToString(AppConfiguration.GetSection("MapFile").Value);
            WindowTop = Convert.ToInt32(AppConfiguration.GetSection("WindowTop").Value);
            WindowLeft = Convert.ToInt32(AppConfiguration.GetSection("WindowLeft").Value);
            WindowWidth = Convert.ToInt32(AppConfiguration.GetSection("WindowWidth").Value);
            WindowHeight = Convert.ToInt32(AppConfiguration.GetSection("WindowHeight").Value);
            BackgroundColor = Convert.ToString(AppConfiguration.GetSection("BackgroundColor").Value);
            FieldSize = Convert.ToInt32(AppConfiguration.GetSection("FieldSize").Value);
            StartX = Convert.ToInt32(AppConfiguration.GetSection("StartX").Value);
            StartY = Convert.ToInt32(AppConfiguration.GetSection("StartY").Value);
            CloneOffsetX = Convert.ToInt32(AppConfiguration.GetSection("CloneOffsetX").Value);
            Delay = Convert.ToInt32(AppConfiguration.GetSection("Delay").Value);
            GroundFloor = Convert.ToInt32(AppConfiguration.GetSection("GroundFloor").Value);
            Credits = Convert.ToString(AppConfiguration.GetSection("Credits").Value);
        }


        public static SetDrawer GetInstance()
        {
            if (_settings == null)
                _settings = new SetDrawer();
            return _settings;
        }


        public void Init(List<Letter> letters)
        {
            GraphicsWindow.Title = Title;
            GraphicsWindow.Top = WindowTop;
            GraphicsWindow.Left = WindowLeft;
            GraphicsWindow.BackgroundColor = BackgroundColor;
            GraphicsWindow.Width = WindowWidth;
            GraphicsWindow.Height = WindowHeight;
            GraphicsWindow.Show();
            _figureIndex = 0;
            GetInstance().SetFigure(new FigureType().items[_figureIndex]);
            WriteControls();
            WriteSerializationInfo();
            List<string[]> map = new List<string[]>();
            MapReader(map);
            for (int i = 0; i < map.Count; i++)
            {
                // Prototype pattern. Создать новый объект - букву
                Letter letter = new Letter().Clone() as Letter;
                // Задать координату X = начальное значение + номер буквы * (ширина буквы + промежуток между буквами)
                letter.SetX(StartX + i * FieldSize * (map[i][0].Length + 1));
                // Расчет координат узлов буквы
                letter.SetFrameNode(map[i]);
                // Сохранить объект - букву
                letters.Add(letter);
            }
            // Задать буквам случайный цвет 
            ChangeColour();
            // Задать буквам случайную фигуру заполнения 
            ChangeFigure();
        }


        /// <summary>
        /// Задать текущий выбор геометрической фигуры для сериализации.
        /// </summary>
        public SetDrawer SetFigure(Figure figure)
        {
            CurrentFigure = figure;
            return this;
        }


        /// <summary>
        /// Надпись с элементами управления.
        /// </summary>
        public void WriteControls()
        {
            GraphicsWindow.BrushColor = "White";
            GraphicsWindow.FontName = "Arial";
            GraphicsWindow.FontSize = 17;
            GraphicsWindow.DrawText(GetInstance().StartX, GroundFloor, "1 - цвет     2 - фигура     3 - клон     4 - выход");
            GraphicsWindow.BrushColor = GetInstance().BackgroundColor;
        }


        /// <summary>
        /// Надпись с выбранным типом сериализации и выбранным типом фигуры.
        /// </summary>
        public static void WriteSerializationInfo()
        {
            GraphicsWindow.FillRectangle(GetInstance().StartX + CloneOffsetX, GroundFloor, 200, 30);
            GraphicsWindow.BrushColor = "White";
            GraphicsWindow.FontName = "Arial";
            GraphicsWindow.FontSize = 17;
            GraphicsWindow.DrawText(GetInstance().StartX + CloneOffsetX, GroundFloor,
                "5 / 6 - " + _serializationType[Program.сurrentType ? 1 : 0] + " / " + GetInstance().CurrentFigure.GetType().Name);
            GraphicsWindow.BrushColor = GetInstance().BackgroundColor;
        }


        /// <summary>
        /// Надпись с именем сохраненного файла сериализованных объектов.
        /// </summary>
        public static void WriteSavedFileName(string fileName)
        {
            GraphicsWindow.FillRectangle(GetInstance().StartX + CloneOffsetX + 220, GroundFloor, WindowWidth - GetInstance().StartX - CloneOffsetX - 220, 30);
            GraphicsWindow.BrushColor = "White";
            GraphicsWindow.FontName = "Arial";
            GraphicsWindow.FontSize = 17;
            GraphicsWindow.DrawText(GetInstance().StartX + CloneOffsetX + 220, GroundFloor, "file: " + fileName);
            GraphicsWindow.BrushColor = GetInstance().BackgroundColor;
        }


        /// <summary>
        /// Титры.
        /// </summary>
        public static void WriteCredits()
        {
            GraphicsWindow.BrushColor = "White";
            GraphicsWindow.FontName = "Arial";
            GraphicsWindow.FontSize = 17;
            GraphicsWindow.DrawText(GetInstance().StartX, GetInstance().StartY, Credits);
        }



        /// <summary>
        /// Обработка события - нажатие клавиши.
        /// </summary>
        public static void GraphicsWindow_KeyDown()
        {
            switch (GraphicsWindow.LastKey.ToString())
            {
                // Изменение цвета буквы случайным образом
                case "D1":
                    ChangeColour();
                    break;
                // Изменение типа фигуры в букве случайным образом
                case "D2":
                    ChangeFigure();
                    break;
                // Клонирование букв
                case "D3":
                    CloneLetters();
                    break;
                // Выход из приложения
                case "D4":
                    Program.gameOver = true;
                    break;
                // Выбор метода сериализации: json/xml
                case "D5":
                    Program.сurrentType = !Program.сurrentType;
                    WriteSerializationInfo();
                    break;
                // Выбор типа фигуры для сериализации объекта с таким типом
                case "D6":
                    if (++_figureIndex == new FigureType().items.Count)
                    {
                        _figureIndex = 0;
                    }
                    //// Builder pattern
                    GetInstance().SetFigure(new FigureType().items[_figureIndex]);
                    WriteSerializationInfo();
                    break;
            }
        }


        /// <summary>
        /// Изменить цвет фигуры.
        /// </summary>
        private static void ChangeColour()
        {
            // Factory method pattern
            new LogicColour().Change();
        }


        /// <summary>
        /// Изменить тип фигуры.
        /// </summary>
        private static void ChangeFigure()
        {
            // Factory method pattern
            new LogicFigure().Change();
        }


        /// <summary>
        /// Создать копии букв по состоянию на текущий момент времени.
        /// </summary>
        private static void CloneLetters()
        {
            lock (Program.letters)
            {
                GraphicsWindow.FillRectangle(WindowWidth / 2, 0, WindowWidth / 2, GroundFloor);
                for (int i = 0; i < Program.letters.Count; i++)
                {
                    // Prototype pattern
                    Letter newOne = Program.letters[i].Clone() as Letter;
                    // Builder pattern
                    newOne.SetX(newOne.LetterX + CloneOffsetX);
                    Program.cloned.Add(newOne);
                }
                Program.isCloned = true;
            }
        }


        /// <summary>
        /// Прочитать геометрию букв из текстового файла.
        /// </summary>
        private void MapReader(List<string[]> map)
        {
            if (!System.IO.File.Exists(MapFile))
            {
                throw new FileNotFoundException($"Файл не найден: {MapFile}");
            }
            string[] fileText = System.IO.File.ReadAllLines(MapFile);
            List<string> letterMap = new List<string>();
            foreach (string line in fileText)
            {
                if (line.Length > 0)
                {
                    letterMap.Add(line);
                }
                else
                {
                    map.Add(letterMap.ToArray());
                    letterMap.Clear();
                }
            }
        }


        /// <summary>
        /// Реализация пользовательского интерфейса клонирования. 
        /// </summary>
        public SetDrawer MyClone()
            => MemberwiseClone() as SetDrawer;
    }
}
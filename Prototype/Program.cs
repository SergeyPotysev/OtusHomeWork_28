﻿//  Использован проект Tetris из курса Otus "C# для начинающих программистов"
//  https://github.com/Kartavec/Tetris/tree/lesson37/Gui-part-3

using Microsoft.SmallBasic.Library;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Prototype
{
    internal class Program
    {
        public static List<Letter> letters = new List<Letter>();
        public static List<Letter> cloned = new List<Letter>();

        /// <summary>
        /// Признак завершения работы. 
        /// </summary>
        public static bool gameOver = false;

        /// <summary>
        /// Признак наличия клонированных объектов.
        /// </summary>
        public static bool isCloned = false;

        /// <summary>
        /// Признак сериализации в json или xml.
        /// </summary>
        public static bool сurrentType = false;

        /// <summary>
        /// Номер файла для сохранения сериализованных объектов.
        /// </summary>
        public static int fileNum = 1;

        private static void Main()
        {
            try
            {
                // Singleton pattern. Инициализация
                SetDrawer.GetInstance().Init(letters);

                // Нарисовать буквы
                foreach (Letter letter in letters)
                    letter.Plot();

                // Регистрация обработчика события - нажатие клавиши
                GraphicsWindow.KeyDown += SetDrawer.GraphicsWindow_KeyDown;

                // Strategy pattern
                IExport[] export = { new ExportJSN(), new ExportXML() };

                int delay = SetDrawer.GetInstance().Delay;
                int y, count;
                count = letters.Count;
                // Цикл анимации букв
                while (true)
                {
                    for (int n = 0; n < count; n++)
                    {
                        letters[n].Hide();
                        if (letters[n].Down)
                        {
                            y = letters[n].LetterY + count - n;
                            if (letters[n].LetterY >= 350)
                                letters[n].SetDown(false);
                        }
                        else
                        {
                            y = letters[n].LetterY - count - n;
                            if (letters[n].LetterY <= 50)
                                letters[n].SetDown(true);
                        }
                        letters[n].SetY(y);
                        letters[n].Plot();
                        Thread.Sleep(delay);
                    }
                    if (isCloned)
                    {
                        for (int n = 0; n < count; n++)
                        {
                            cloned[n].Plot();
                        }
                        // Visitor pattern. Сериализовать объект-букву с применением двойной диспетчеризации 
                        SetDrawer.GetInstance().CurrentFigure.Export(export[сurrentType ? 1 : 0]);
                        cloned.Clear();
                        isCloned = false;
                    }
                    if (gameOver)
                        break;
                }
                GraphicsWindow.Clear();
                SetDrawer.WriteCredits();
                // Неполное клонирование с пользовательским интерфейсом
                var myClone = SetDrawer.GetInstance().MyClone().GetType().ToString();
                Console.WriteLine(myClone);               
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Environment.Exit(0);
            }
        }

    }
}
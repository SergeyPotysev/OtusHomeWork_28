﻿namespace Prototype
{
    class LogicFigure : Logic
    {
        protected override Parameter CreateParameter()
        {
            return new FigureType();
        }
    }
}

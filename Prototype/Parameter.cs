﻿using System;

namespace Prototype
{
    [Serializable]
    abstract class Parameter : DeepClone
    {
        public abstract void Change();
    }
}

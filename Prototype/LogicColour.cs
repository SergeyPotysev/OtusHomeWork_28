﻿namespace Prototype
{
    class LogicColour : Logic
    {
        protected override Parameter CreateParameter()
        {
            return new ColourType();
        }
    }
}

﻿namespace Prototype
{
    abstract class Logic
    {
        abstract protected Parameter CreateParameter();

        public void Change()
        {
            Parameter parameter = CreateParameter();
            parameter.Change();
        }
    }
}

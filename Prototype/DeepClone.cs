﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Prototype
{
    [Serializable]
    public class DeepClone : ICloneable
    {
        /// <summary>
        /// Полное копирование (Deep copy).
        /// </summary>
        /// <returns>Клонированный объект.</returns>
        public object Clone()
        {
            object obj = null;
            using (MemoryStream tempStream = new MemoryStream())
            {
                BinaryFormatter binFormatter = new BinaryFormatter(null, new StreamingContext(StreamingContextStates.Clone));
                binFormatter.Serialize(tempStream, this);
                tempStream.Seek(0, SeekOrigin.Begin);
                obj = binFormatter.Deserialize(tempStream);
            }
            return obj;
        }

    }
}

﻿using System.IO;
using System.Linq;

namespace Prototype
{
    class ExportXML : IExport
    {

        public void Export(Rectangle figure)
        {
            ExportLetter(figure);
        }


        public void Export(Ellipse figure)
        {
            ExportLetter(figure);
        }


        public void Export(Rhombus figure)
        {
            ExportLetter(figure);
        }


        public void Export(Triangle figure)
        {
            ExportLetter(figure);
        }


        public void Export(Figure figure)
        {
        }


        public void ExportLetter(Figure figure)
        {
            Letter letter = Program.cloned.Where(x => x.Figure.GetType() == figure.GetType()).First();
            string xmlString = string.Empty;
            string crlf = "\r\n";
            string space = "    ";
            string[] figureName = letter.Figure.ToString().Split(new char[] { '.' });
            xmlString += "<letter>" + crlf;
            xmlString += space + $"<figure>{figureName[figureName.Count() - 1]}</figure>" + crlf;
            xmlString += space + $"<colour>{letter.Colour}</colour>" + crlf;
            xmlString += space + $"<X>{letter.LetterX}</X>" + crlf;
            xmlString += space + $"<Y>{letter.LetterY}</Y>" + crlf;
            xmlString += space + $"<nodes>" + crlf;
            foreach (var node in letter.frameNodes)
            {
                xmlString += space + space + "<node>" + crlf;
                xmlString += space + space + space + $"<x>{node.X}</x>" + crlf;
                xmlString += space + space + space + $"<y>{node.Y}</y>" + crlf;
                xmlString += space + space + "</node>" + crlf;
            }
            xmlString += space + "</nodes>" + crlf;
            xmlString += "</letter>" + crlf;
            string fileName = "cloned_" + Program.fileNum++.ToString() + ".xml";
            File.WriteAllText(fileName, xmlString);
            SetDrawer.WriteSavedFileName(fileName);
        }

    }
}

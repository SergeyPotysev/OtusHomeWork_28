﻿namespace Prototype
{
    interface IExport
    {
        void Export(Figure figure);
        void Export(Rectangle figure);
        void Export(Ellipse figure);
        void Export(Rhombus figure);
        void Export(Triangle figure);
        void ExportLetter(Figure figure); 
    }
}

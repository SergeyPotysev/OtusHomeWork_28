﻿using System.IO;
using System.Linq;

namespace Prototype
{
    class ExportJSN : IExport
    {
        public void Export(Rectangle figure)
        {
            ExportLetter(figure);
        }

        public void Export(Ellipse figure)
        {
            ExportLetter(figure);
        }

        public void Export(Rhombus figure)
        {
            ExportLetter(figure);
        }

        public void Export(Triangle figure)
        {
            ExportLetter(figure);
        }


        public void Export(Figure figure)
        {
        }


        public void ExportLetter(Figure figure) 
        {
            Letter letter = Program.cloned.Where(x => x.Figure.GetType() == figure.GetType()).First();
            string jsonString = string.Empty;
            string crlf = "\r\n";
            string space = "    ";
            string[] figureName = letter.Figure.ToString().Split(new char[] { '.' });
            jsonString += @"{""letter"": {" + crlf;
            jsonString += space + $@"""figure"": ""{figureName[figureName.Count() - 1]}""," + crlf;
            jsonString += space + $@"""colour"": ""{letter.Colour}""," + crlf;
            jsonString += space + $@"""X"": ""{letter.LetterX}""," + crlf;
            jsonString += space + $@"""Y"": ""{letter.LetterY}""," + crlf;
            jsonString += space + $@"""nodes"": [" + crlf;
            foreach (var node in letter.frameNodes)
            {
                jsonString += space + space + $@"{{""x"": {node.X}, ""y"": {node.Y}}}," + crlf;
            }
            jsonString = jsonString.Remove(jsonString.Length - 3) + crlf;
            jsonString += space + "]" + crlf;
            jsonString += "}}" + crlf;
            string fileName = "cloned_" + Program.fileNum++.ToString() + ".json";
            File.WriteAllText(fileName, jsonString);
            SetDrawer.WriteSavedFileName(fileName);
        }
 
    }
}

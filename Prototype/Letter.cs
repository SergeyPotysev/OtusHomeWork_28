﻿using Microsoft.SmallBasic.Library;
using System;
using System.Collections.Generic;

namespace Prototype
{
    [Serializable]
    class Letter : DeepClone
    {
        /// <summary>
        /// Цвет буквы.
        /// </summary>
        public string Colour { get; private set; }

        /// <summary>
        /// Тип геометрической фигуры для заполнения полей буквы.
        /// </summary>
        public Figure Figure { get; private set; }

        /// <summary>
        /// Координата верхнего левого угла буквы относительно вернего левого угла окна по оси X.
        /// </summary>
        public int LetterX { get; private set; }

        /// <summary>
        /// Координата верхнего левого угла буквы относительно вернего левого угла окна по оси Y.
        /// </summary>
        public int LetterY { get; private set; }

        /// <summary>
        /// Поличество полей по высоте буквы.
        /// </summary>
        public int Rows { get; private set; }

        /// <summary>
        /// Поличество полей по ширине буквы.
        /// </summary>
        public int Columns { get; private set; }

        /// <summary>
        /// Размер поля.
        /// </summary>
        public int FieldSize { get; private set; }

        /// <summary>
        /// Указатель направления движения.
        /// </summary>
        public bool Down { get; private set; }

        /// <summary>
        /// Координаты узловых точек буквы.
        /// </summary>
        public List<Point> frameNodes = new List<Point>();


        public Letter()
        {
            // Singleton pattern
            Colour = SetDrawer.GetInstance().BackgroundColor;
            LetterX = SetDrawer.GetInstance().StartX;
            LetterY = SetDrawer.GetInstance().StartY;
            FieldSize = SetDrawer.GetInstance().FieldSize;
            Down = true; 
        }


        /// <summary>
        /// Рассчитать координаты фрейма буквы.
        /// </summary>
        /// <param name="map"></param>
        public void SetFrameNode(string[] map)
        {
            int x, y;
            Point newOne;
            Rows = map.GetUpperBound(0) + 1;
            Columns = map[0].Length;
            for (int i = 0; i < Rows; i++)
            {
                for (int n = 0; n < Columns; n++)
                {
                    if (map[i][n] == '1')
                    {
                        x = n * FieldSize;
                        y = i * FieldSize;
                        // Prototype pattern
                        newOne = new Point().MyClone(); 
                        // Builder pattern
                        newOne.SetX(x).SetY(y);
                        frameNodes.Add(newOne);
                    }
                }
            }
        }


        /// <summary>
        /// Закрасить площадь буквы цветом BackgroundColor.
        /// </summary>
        public void Hide()
        {
            GraphicsWindow.FillRectangle(LetterX - 2, LetterY - 2, Columns * FieldSize + 4, Rows * FieldSize + 4);
        }


        /// <summary>
        /// Задать букве цвет.
        /// </summary>
        /// <param name="colour">Строковое значение цвета.</param>
        public Letter SetColor(string colour)
        {
            Colour = colour;
            return this;
        }


        /// <summary>
        /// Изменить направление движения.
        /// </summary>
        public Letter SetDown(bool value)
        {
            Down = value;
            return this;
        }


        /// <summary>
        /// Задать тип геометрической фигуры.
        /// </summary>
        /// <param name="figure">Тип фигуры для заполнения полей буквы.</param>
        public Letter SetFigure(Figure figure)
        {
            Figure = figure;
            return this;
        }


        /// <summary>
        /// Задать букве начальную координату по оси X.
        /// </summary>
        /// <param name="x">Новое значение координаты по оси X.</param>
        public Letter SetX(int x)
        {
            LetterX = x;
            return this;
        }


        /// <summary>
        /// Задать букве начальную координату по оси Y.
        /// </summary>
        /// <param name="y">Новое значение координаты по оси Y.</param>
        public Letter SetY(int y)
        {
            LetterY = y;
            return this;
        }


        /// <summary>
        /// Заполнить поля буквы заданной геометрической фигурой.
        /// </summary>
        public void Plot()
        {
            GraphicsWindow.PenColor = Colour;
            foreach (Point point in frameNodes)
            {
                Figure.SetX(LetterX + point.X);
                Figure.SetY(LetterY + point.Y);
                Figure.Plot();
            }
        }

    }
}